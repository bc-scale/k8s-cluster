# k8s-cluster
*WIP*

The #1 goal of this repo is to provide a manual implementation of general-use 
features first. After that initial documentation exists, we can automate it.

My reasoning is, this basic stand-up process needs to be understood via the native CLIs first so they will be
more useful during development, testing, and incidents. By no means does that mean I won't _automate all the things_. :D

I was recently asked why I chose [Linkerd2](https://linkerd.io) over [Istio](https://istio.io/): 
 * Linkerd2
   * 'Incubating' project within the [CNCF](https://www.cncf.io/)
   * ["Committed to Open Governance"](https://linkerd.io/2019/10/03/linkerds-commitment-to-open-governance/)
   * [Multi-Cluster Support](https://linkerd.io/2/features/multicluster_support/)
   * Re-written in [Rust](https://www.rust-lang.org/) for v2 (v1 was originally written in Scala)
 * Itsio
   * Written in [Go](https://golang.org/) (not that there's anything wrong with that)
   * Utilizes [Envoy Proxy](https://www.envoyproxy.io/), a CNCF 'graduated' project started by Lyft
   * Direction is primarily controlled by Google, along with IBM and Lyft
   * Not part of the CNCF (yet)

On top of making this a good reference, when I am choosing what project I'm going to look at next, 
it will likely include CNCF supported projects over non. By doing so, I hOpenTracingope to have a well 
supported and compatible system that can be deployed anywhere.

-- Sarah Bennert  
https://gitlab.com/sarahbx/k8s-cluster

#### Kubernetes Versions Supported
* v1.16.x

#### Clouds Supported
* [DigitalOcean](https://digitalocean.com) (DOKS)
  * Minimum node size: `s-2vcpu-4gb`

#### Summary
* Basic Managed [Kubernetes](https://kubernetes.io) 3 node cluster deployment.
* Volumes added dynamically for persistent storage.
* Utilizes [Linkerd](https://linkerd.io) and [Let's Encrypt](https://letsencrypt.org)

# Table of Contents
* [Templates](#templates)

#### Initial Deployment and Provisioning
* [Local Dependencies](#local-dependencies)
* [Deploy Cluster](./docs/k8s/README.md#deploy-cluster)

#### Install Service Mesh and Cert-Manager
* [Install Linkerd on Cluster](./docs/k8s/README.md#install-linkerd-on-cluster)
* [Install Dummy Services for Testing](./docs/k8s/README.md#install-dummy-services-for-testing)
* [Ingress](./docs/k8s/README.md#ingress)
  * [Deploy Mandatory Manifest](./docs/k8s/README.md#deploy-mandatory-manifest)
  * [Install Cloud Ingress](./docs/k8s/README.md#install-ingress)
  * [Bare Metal Ingress (NodePort)](./docs/k8s/README.md#bare-metal-ingress-nodeport)
* [Deploy Test Ingress](./docs/k8s/README.md#deploy-test-ingress)
* [Deploy Cert-Manager](./docs/k8s/README.md#deploy-cert-manager)
* [Staging](./docs/k8s/README.md#staging)
* [Production](./docs/k8s/README.md#production)
* [Add DigitalOcean container registry](./docs/k8s/README.md#add-digitalocean-container-registry)
* [Deploy your services to the mesh](./docs/k8s/README.md#deploy-your-services-to-the-mesh)

#### Install Additional Observability
* [Linkerd connected OpenTelemetry (OpenSensus/OpenCollector) and Jaeger](./docs/k8s/linkerd-opentracing/README.md)
* [Honeycomb Integration](./docs/k8s/honeycomb/README.md)
* Cluster-wide Logging via [ElasticSearch, Kibana, and Fluentd](./docs/k8s/logging/README.md)

# Templates
Templates will be created at a later time for easier use.  
Until then, please search for `exampledomain` in this repo for places where you may need to make changes.

# Local Dependencies
*Provisioning system was a Ubuntu 20.04 derivative*

### Terraform CLI
Version Used: `v0.12.8`
https://www.terraform.io/downloads.html

### Kubectl CLI
Use version matching cluster version to be deployed:  
https://kubernetes.io/docs/tasks/tools/install-kubectl/

### Linkerd Service Mesh CLI
https://linkerd.io/2/getting-started/
```
curl -sL https://run.linkerd.io/install | sh
ln -s ~/.linkerd2/bin/linkerd ~/bin/
linkerd version
linkerd check --pre
```

### Doctl CLI
Version Used: 1.41.0
https://www.digitalocean.com/docs/apis-clis/doctl/how-to/install/
```
sudo snap install doctl
```
