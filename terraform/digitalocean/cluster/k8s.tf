resource "digitalocean_kubernetes_cluster" "do-k8s-cluster-01" {
  name    = "do-k8s-cluster-01"
  tags    = ["do-k8s-cluster-01"]
  region  = "${var.do_region}"
  version = "${var.do_k8s_version}"

  node_pool {
    name       = "do-k8s-cluster-01-${var.do_k8s_node_pool_prefix}-01"
    size       = "${var.do_k8s_node_size}"
    auto_scale = true
    node_count = "${var.do_k8s_node_count}"
    min_nodes  = "${var.do_k8s_node_min}"
    max_nodes  = "${var.do_k8s_node_max}"
    tags       = ["do-k8s-cluster-01"]
  }
  vpc_uuid = "${local.do_vpc_id}"
}