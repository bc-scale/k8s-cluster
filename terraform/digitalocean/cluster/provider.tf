provider "digitalocean" {
  token = "${var.do_token}"
}

data digitalocean_vpc "k8s-cluster-testing" {
  name = "${local.do_vpc_name}"
}

data digitalocean_project "k8s-cluster-testing" {
  name = "${local.do_vpc_name}"
}

resource "digitalocean_project_resources" "k8s-cluster-testing-resources" {
  project = data.digitalocean_project.k8s-cluster-testing.id
  resources = []
}