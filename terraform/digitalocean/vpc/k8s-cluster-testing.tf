resource "digitalocean_project" "k8s-cluster-testing" {
  name        = "k8s-cluster-testing"
  description = "Kubernetes Deployment Testing"
  purpose     = "Kubernetes Deployment Testing"
  environment = "Development"
}

resource "digitalocean_vpc" "k8s-cluster-testing" {
  name     = "${var.do_vpc_name}"
  region   = "${var.do_region}"
  ip_range = "10.10.10.0/24"
}