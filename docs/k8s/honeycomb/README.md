# Honeycomb - honeycomb.io
https://docs.honeycomb.io/getting-data-in/integrations/kubernetes/configuration/
## Service Installation
```
YOUR_API_KEY=''
kubectl create secret generic -n kube-system honeycomb-writekey --from-literal=key="${YOUR_API_KEY}"

curl -L https://honeycomb.io/download/kubernetes/metrics/honeycomb-heapster.yaml > k8s/honeycomb/honeycomb-heapster.yaml
linkerd inject k8s/honeycomb/honeycomb-heapster.yaml | kubectl apply -f -

https://honeycomb.io/download/kubernetes/logs/quickstart.yaml
curl -L https://honeycomb.io/download/kubernetes/logs/quickstart.yaml > k8s/honeycomb/quickstart.yml
sed -i 's/^\([ \t]*\)namespace: default[ \t]*$/\1namespace: exampledomain-prod/g' k8s/honeycomb/quickstart.yml
linkerd inject k8s/honeycomb/quickstart.yml | kubectl -n exampledomain-prod create -f -

```
## Agent Installation
```
(echo -e "---\nwritekey: ${YOUR_API_KEY}" && cat k8s/honeycomb/config.yaml.tmpl) > k8s/honeycomb/config.yaml

kubectl -n exampledomain-prod create configmap honeycomb-agent-config --from-file=k8s/honeycomb/config.yaml


kubectl -n exampledomain-prod create configmap honeycomb-agent-config \
    --from-file=k8s/honeycomb/config.yaml --output=yaml \
    --dry-run | kubectl replace --filename=-

kubectl -n exampledomain-prod delete pod --selector app=honeycomb-agent
```
