# Linkerd Distributed Tracing

https://linkerd.io/2/tasks/distributed-tracing/

# Install OpenCensus Collector
OpenTelemetry is still in beta, so until that's released, we'll use OpenCensus

https://opencensus.io/

https://opentelemetry.io/about/
```
curl https://run.linkerd.io/tracing/collector.yml > k8s/linkerd-opentracing/collector.yml
linkerd inject k8s/linkerd-opentracing/collector.yml | kubectl apply -f -
kubectl -n tracing rollout status deploy/oc-collector
```

# Install Jaeger
https://www.jaegertracing.io/
```
curl https://run.linkerd.io/tracing/backend.yml > k8s/linkerd-opentracing/backend.yml
linkerd inject k8s/linkerd-opentracing/backend.yml | kubectl apply -f -
kubectl -n tracing rollout status deploy/jaeger
```

# Testing
Deploy emojivoto test service to provide trace data
```
kubectl apply -f https://run.linkerd.io/emojivoto.yml
kubectl -n emojivoto patch -f https://run.linkerd.io/emojivoto.yml -p '
spec:
  template:
    metadata:
      annotations:
        linkerd.io/inject: enabled
        config.linkerd.io/trace-collector: oc-collector.tracing:55678
'
kubectl -n emojivoto rollout status deploy/web
kubectl -n emojivoto set env --all deploy OC_AGENT_HOST=oc-collector.tracing:55678
```

# Jaeger UI
Visualize and explore trace data
```
kubectl -n tracing port-forward svc/jaeger 16686
```
http://localhost:16686/
